<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Editar proyecto</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
</head>
    <body>
        <h1>Hello,</h1>

        @foreach ($proyectos as $proyecto)
            <li>{{$proyecto->name}}</li>
            <a href = "{{ route('proyecto.edit', $proyecto->id) }}" class="btn btn-info" style="margin-right: 5px;color: white;"> Editar 
  </a>
   <form action="{{ route('proyecto.destroy', $proyecto->id) }}" method="post" style="display:inline;">
    @csrf
    @method('DELETE')
    <button type="submit" class="btn btn-danger">Borrar</button>
  </form>
        @endforeach
    </body>
</html>



